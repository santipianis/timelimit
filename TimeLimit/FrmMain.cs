﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeLimit
{
    public partial class FrmMain : Form
    {
        private TimeManager TimeManager { get; set; }
        private bool Started { get; set; } = false;
        public FrmMain()
        {
            InitializeComponent();
            TimeManager = new TimeManager();
            TimeManager.Tick += Tick;
        }

        private void Tick(object sender, EventArgs e)
        {
            var time = TimeManager.GetLeftTime();
            DisplayLeftTime(time.Hours, time.Minutes);
        }

        private void DisplayLeftTime(int hours, int minutes)
        {
            
            var hoursStr = hours.ToString();
            if (hours < 10)
            {
                hoursStr = $"0{hoursStr}";
            }
            var minutesStr = minutes.ToString();
            if (minutes < 10)
            {
                minutesStr = $"0{minutesStr}";
            }
            lblTimeDisplay.Text = $"{hoursStr}:{minutesStr}";
        }

        private void BtnSet_Click(object sender, EventArgs e)
        {
            int.TryParse(txtHours.Text, out int hours);
            int.TryParse(txtMinutes.Text, out int minutes);
            if (hours == 0 && minutes == 0 && !Started)
            {
                return;
            }

            Started = !Started;
            if (Started)
            {
                TimeManager.SetTimer(hours, minutes);
                DisplayLeftTime(hours, minutes);
                btnSet.Text = "Stop";
                txtHours.Text = string.Empty;
                txtMinutes.Text = string.Empty;
            }
            else
            {
                TimeManager.StopTimer();
                btnSet.Text = "Set Time";
            }
            HideShowSetFields();
        }

        private void HideShowSetFields()
        {
            txtHours.Visible = !txtHours.Visible;
            txtMinutes.Visible = !txtMinutes.Visible;
            lblHours.Visible = !lblHours.Visible;
            lblMinutes.Visible = !lblMinutes.Visible;
            lblTimeDisplay.Visible = !lblTimeDisplay.Visible;
        }

        private void FrmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                notifyIcon.Visible = true;
                Hide();
            }
            else
            {
                notifyIcon.Visible = false;
                Show();
            }
         }

        private void NotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }
    }
}
