﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace TimeLimit
{
    public class TimeManager
    {
        private System.Windows.Forms.Timer Timer { get; set; }
        private Time LeftTime { get; set; }
        private DateTime StartDateTime { get; set; }
        private int TickCount { get; set; } = 0;

        public event EventHandler Tick;

        public TimeManager()
        {

            Timer = new System.Windows.Forms.Timer
            {
                Interval = 1000 // 1 second
            };
            Timer.Tick += TickTak;
        }

        protected void OnTick(object sender, EventArgs e)
        {
            TickCount++;
            if (TickCount == 60)
            {
                if (LeftTime.Minutes > 1)
                {
                    LeftTime.Minutes--;
                    TickCount = 0;
                }
                else
                {
                    if (LeftTime.Hours > 0)
                    {
                        LeftTime.Hours--;
                        LeftTime.Minutes = 59;
                        TickCount = 0;
                    }
                    else
                    {
#if DEBUG
                        Timer.Stop();
                        return;
#else
                        Shutdown();
#endif
                    }
                }
            }
            Tick?.Invoke(this, e);
        }

        public Time GetLeftTime()
        {
            return LeftTime;
        }

        private void TickTak(object sender, EventArgs e)
        {
            
            OnTick(sender,e);
        }
        void Shutdown()
        {
            ManagementBaseObject mboShutdown = null;
            var mcWin32 = new ManagementClass("Win32_OperatingSystem");
            mcWin32.Get();

            // You can't shutdown without security privileges
            mcWin32.Scope.Options.EnablePrivileges = true;
            var mboShutdownParams = mcWin32.GetMethodParameters("Win32Shutdown");

            // Flag 1 means we want to shut down the system. Use "2" to reboot.
            mboShutdownParams["Flags"] = "1";
            mboShutdownParams["Reserved"] = "0";
            foreach (ManagementObject manObj in mcWin32.GetInstances())
            {
                mboShutdown = manObj.InvokeMethod("Win32Shutdown",
                                               mboShutdownParams, null);
            }
        }

        internal void SetTimer(int hours, int minutes)
        {
            if(hours > 24)
            {
                throw new Exception("Max value for hours is 24");
            }
            if (minutes > 60)
            {
                throw new Exception("Max value for minutes is 60");
            }
            if (hours < 0)
            {
                hours = 0;
            }
            if (minutes < 0)
            {
                minutes = 0;
            }
            if (hours<=0 && minutes <= 0)
            {
                return;
            }
            LeftTime = new Time
            {
                Hours = hours,
                Minutes = minutes
            };
            StartDateTime = DateTime.Now;
            Timer.Start();
        }

        public void StopTimer()
        {
            Timer.Stop();
        }

        public void PauseTimer()
        {
            
        }
    }
}
