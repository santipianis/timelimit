﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeLimit
{
    public class Time
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
    }
}
